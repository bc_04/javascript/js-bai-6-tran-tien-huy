let main = () => {
  let bai_1 = () => {
    /**
     * Input:
     * - a = 3
     * - b = 10
     * - c = 5
     * - benchmark = 12
     * - location_type_point = 0.5
     * - student_type_point = 1
     * Steps:
     *  - Declare and Initialize 6 variables to store 6 inputs.
     *  - Declare and Initialize a varible "sum" to store total sum
     *  - Declare and Initialize a variable to store output.
     *  - If any of (a,b,c) is 0 => No need to calculate => print out message "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0"
     *  - Else:
     *      + sum = a+b+c+location_type_point+student_type_point
     *      + Compare sum with benchmark.
     *         => If sum >= benchmark => Đậu
     *         => Else, rớt.
     * Output: Bạn đã đậu. Tổng điểm: 18
     *
     */
    let i,
      even_list = "",
      odd_list = "";
    let output = "";
    for (i = 0; i < 100; i++) {
      !(i % 2) ? (even_list += `<li>${i}</li>`) : (odd_list += `<li>${i}</li>`);
    }
    output = `
      <div class="even-numb-list">
        <h4>Danh sách số chẵn nhỏ hơn 100 </h4>
        <ul>${even_list}</ul>
      </div>
      <div class="odd-numb-list">
        <h4>Danh sách số lẻ nhỏ hơn 100 </h4>
        <ul>${odd_list}</ul>
      </div>
    `;
    document.querySelectorAll(".txt-result-bai-1")[0].innerHTML = output;
  };

  let bai_2 = () => {
    /**
     * Input:
     * - user_name = Huy;
     * - electricity_number = 123
     * Steps:
     *  - Declare and Initialize 3 variables to store 2 inputs.
     *  - Declare and Initialize a variable to store electricity bill.
     *  - Declare and Initialize a variable to store output.
     *  if(electricity_number <= 50) {
     *    electricity_bill = 500 * electricity_number;
     *  } else if(electricity_number <= 100) {
     *    electricity_bill = 650 * (electricity_number - 50) + 500 * 50;
     *  } else if(electricity_number <= 200) {
     *    electricity_bill = 850 * (electricity_number - 100) + 50*650 + 50*500;
     *  } else if(electricity_number <=350) {
     *    electricity_bill = 1100 * (electricity_number - 200) + 100 * 850 + 50*650 + 50*500;
     *  } else {
     *    electricity_bill = 1300 * (electricity_number-350) + 1100 * 150 + 100 * 850 + 50*650 + 50*500;
     *  }
     *
     *
     * Output: Họ tên: Huy; Tiền điện: 77050
     *
     */
    let i,
      counter = 0;
    for (i = 0; i <= 1000; i++) {
      !(i % 3) && counter++;
    }
    output = `Số lượng số chia hết cho 3 dưới 1000: ${counter}`;
    document.querySelectorAll(".txt-result-bai-2")[0].innerHTML = output;
  };

  let bai_3 = () => {
    let i = 1,
      sum = 0;
    for (sum = 0; sum <= 10000; sum += i) {
      i++;
    }
    output = `Số nguyên dương nhỏ nhất sao cho tổng dãy số > 10000: ${i}`;
    document.querySelectorAll(".txt-result-bai-3")[0].innerHTML = output;
  };

  let bai_4 = () => {
    let input_x = parseInt(document.querySelectorAll(".inputX")[0].value) || 0;
    let input_n = parseInt(document.querySelectorAll(".inputN")[0].value) || 0;
    let i,
      sum = 0;
    let output = "";
    if (input_x) {
      if (!input_n) {
        sum = 1;
      } else {
        for (i = 1; i <= input_n; i++) {
          sum = Math.pow(input_x, i);
        }
      }
    }
    output = `Tổng: ${sum}`;
    document.querySelectorAll(".txt-result-bai-4")[0].innerHTML = output;
  };

  let bai_5 = () => {
    let n =
      parseInt(document.querySelectorAll(".bai-tap-5 .inputN")[0].value) || 0;
    let factorial = 1,
      output = "";
    if (n > 0) {
      for (i = 1; i <= n; i++) {
        factorial *= i;
      }
    }
    output = `Giai thừa: ${factorial}`;
    document.querySelectorAll(".txt-result-bai-5")[0].innerHTML = output;
  };

  let bai_6 = () => {
    let i = 0,
      style_class_name = "",
      output = "";
    for (i = 0; i < 10; i++) {
      style_class_name = "bg-primary";
      !(i % 2) && (style_class_name = "bg-danger");
      output += `<div class="${style_class_name} text-white p-2">Div ${i}</div>`;
    }
    document.querySelectorAll(".txt-result-bai-6")[0].innerHTML = output;
  };
  let bai_7 = () => {
    let i = 2,
      j,
      flag = 1;
    let n =
      parseInt(document.querySelectorAll(".bai-tap-7 .inputN")[0].value) || 0;
    let output = "";
    if (n < 2) {
      output = "Số bạn nhập vào không hợp lệ";
    } else if (n == 2) {
      output = "2";
    } else {
      for (i = 2; i <= n; i++) {
        flag = 1;
        for (j = 2; j <= Math.sqrt(i); j++) {
          if (!(i % j)) {
            flag = 0;
            break;
          }
        }

        flag && (output += `${i} `);
      }
    }
    document.querySelectorAll(".txt-result-bai-7")[0].innerHTML = output;
  };
  document
    .querySelectorAll(".btn-calc-bai-1")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_1();
    });

  // giai bai 2
  document
    .querySelectorAll(".btn-calc-bai-2")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_2();
    });

  // giai bai 3
  document
    .querySelectorAll(".btn-calc-bai-3")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_3();
    });

  // giai bai 4
  document
    .querySelectorAll(".btn-calc-bai-4")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_4();
    });

  // giai bai 5
  document
    .querySelectorAll(".btn-calc-bai-5")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_5();
    });

  // giai bai 6
  document
    .querySelectorAll(".btn-calc-bai-6")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_6();
    });

  // giai bai 7
  document
    .querySelectorAll(".btn-calc-bai-7")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_7();
    });
};

main();
